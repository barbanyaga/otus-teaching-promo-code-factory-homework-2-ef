﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Model.Configuration
{
    public class CustomerConfiguration: IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder
                .Property(e => e.FirstName).HasMaxLength(80);
            builder
                .Property(e => e.LastName).HasMaxLength(120);
            builder
                .Property(e => e.Email).HasMaxLength(320);
            builder
                .HasMany(e => e.PromoCodes)
                .WithOne(pc => pc.Customer)
                .HasForeignKey(promoCode => promoCode.CustomerId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasData(FakeDataFactory.Customers);
        }
    }
}