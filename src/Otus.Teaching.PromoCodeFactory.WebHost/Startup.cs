using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Model;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using SmartBuilding.MobileApi.Extensions.Infrastructure;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<PromoDbContext>();

            services.AddScoped(typeof(IRepository<Employee>), s =>
            {
                var dbContext = s.GetService<PromoDbContext>();
                return new EfRepository<Employee>(dbContext);
            });

            services.AddScoped(typeof(IRepository<Role>), s =>
            {
                var dbContext = s.GetService<PromoDbContext>();
                return new EfRepository<Role>(dbContext);
            });
            
            services.AddScoped(typeof(IRepository<Preference>), s =>
            {
                var dbContext = s.GetService<PromoDbContext>();
                return new EfRepository<Preference>(dbContext);
            });

            services.AddScoped(typeof(IRepository<CustomerPreference>), s =>
            {
                var dbContext = s.GetService<PromoDbContext>();
                return new EfRepository<CustomerPreference>(dbContext);
            });
            
            services.AddScoped(typeof(IRepository<Customer>), s =>
            {
                var dbContext = s.GetService<PromoDbContext>();
                return new EfRepository<Customer>(dbContext);
            });

            services.MigrateDataBase();

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });

            services.AddAutoMapper(typeof(Startup));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x => { x.DocExpansion = "list"; });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            // dbInitializer.Init();
        }
    }
}