﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;
        private readonly IMapper _mapper;

        public CustomersController(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<CustomerPreference> customerPreferenceRepository,
            IMapper mapper)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var customersResponse =
                _mapper.Map<List<CustomerShortResponse>>(customers.ToList());
            return Ok(customersResponse);
        }

        /// <summary>
        /// Получить клиента по идентифкиатору
        /// </summary>
        /// <param name="id">Идентфифкатор клиента</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.FirstOrDefaultWithInclude(
                c => c.Id == id,
                c => c.CustomerPreferences);

            var preferences = await _preferenceRepository.GetAllAsync();

            var customerResponse =
                _mapper.Map<CustomerResponse>(customer);
            customerResponse.Preferences = new List<PreferenceResponse>();

            foreach (var customerPreference in customer.CustomerPreferences)
            {
                var preference = preferences.FirstOrDefault(p => p.Id == customerPreference.PreferenceId);
                customerResponse.Preferences.Add(new PreferenceResponse()
                {
                    Id = preference!.Id,
                    Name = preference.Name
                });
            }

            return Ok(customerResponse);
        }

        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <param name="request">Запрос на создание клиента</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var newCustomer = new Customer
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                CustomerPreferences = request.PreferenceIds
                    .Select(preferenceId => new CustomerPreference
                    {
                        PreferenceId = preferenceId,
                    }).ToList()
            };
            await _customerRepository.CreateAsync(newCustomer);

            return Ok();
        }

        /// <summary>
        /// Обновить данные клиента
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <param name="request">Запрос на редактирование данные клиента</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            // TODO - проставлять предпочтения пользователя
            customer.CustomerPreferences = new List<CustomerPreference>();

            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id">Идентификатор клиета</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _customerRepository.DeleteByIdAsync(id);
            return Ok();
        }
    }
}